FROM ubuntu:devel

RUN apt-get update && apt-get install -y --no-install-recommends \
    ntp \
 && rm -rf /var/lib/apt/lists/*

COPY ntp.conf /etc

EXPOSE 123/udp
CMD ["ntpd", "-n"]

